(function(){

	// Create a Module named gemStore so we can get started on this marketing journey.
	var app = angular.module('gemStore', ['store-directives']);

    // Add a controller named StoreController to our gemStore application.
    app.controller('StoreController', ['$http', function($http) {
            var store = this;

            store.products = [];

            $http.get('/Angular/Codeschool/Flatlanders//store-products.json').success(function(data) {
                store.products = data;
            });
        }
    ]);

    // ReviewController
    app.controller('ReviewController', function(){

        // Set our review variable to an empty object when the ReviewController is created.
        this.review = {};

        // Create an empty function named addReview in your ReviewController
        //  pass in the product we want to review to our addReview function
        this.addReview = function(product) {

            // Before the review is pushed onto the array,
            // add a new key to the review namedcreatedOn and value Date.now().
            this.review.createdOn = Date.now();

            // When addReview is called with a product, it should add the review from the controller
            // to the passed in products reviews array. Implement this functionality in the addReview method.
            product.reviews.push(this.review);

            // Reset the review to an empty object after it's been added to product.reviews.
            this.review = {};
        };


    });
})();
