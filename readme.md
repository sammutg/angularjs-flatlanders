A simple prototype built using AngularJS and Twitter Bootstrap that shows a sample module of cars showcase and their information. To build this project, I have implemented the concepts of Angular JS's

Built-in directives,
Forms,
Custom directives,
Services,
Dependency injection,
Refactoring everything into individual modules.