(function(){

    // Create a new Module named store-directives to encapsulate our store Directives.
    var app = angular.module('store-directives', []);


    // Move the Directive definitions from app.js into products.js.
    app.directive("productTabs", function() {
      return {
        restrict: 'E',
        templateUrl: "product-tabs.html",
        controller : function(){
            this.tab = 1;

            this.isSet = function(checkTab) {
              return this.tab === checkTab;
            };

            this.setTab = function(setTab) {
              this.tab = setTab;
            };
        },
        controllerAs : 'tab',
      };
    });


    // Create an element directive called productGallery.
    app.directive("productGallery", function() {
        return {
            restrict: "E",
            templateUrl : "product-gallery.html",
            controller : function(){

                this.current = 0;

                this.setCurrent = function(newGallery){
                  this.current = newGallery || 0; //  If no value is passed in, set current to 0.
                };
            },
            controllerAs : "gallery",
        };
    });


   // Instead of using ng-include, create a custom directive called productDescription.
    // Create an Element Directive for our Description div that includes the product-description.html.
    app.directive('productDescription', function(){
        return {
            restrict: 'E',
            templateUrl: "product-description.html"
        };
    });

    app.directive("productReviews", function() {
        return {
            restrict: 'E',
            templateUrl: "product-reviews.html"
        };
    });


    // Create a new attribute directive for our specs tag called productSpecs.
    // Have it use our new product-specs.html template.
    app.directive("productSpecs", function() {
        return {
          restrict: 'A',
          templateUrl: "product-specs.html"
        };
    });

})();
